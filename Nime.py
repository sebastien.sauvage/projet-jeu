from random import *

def installation_jeu ():
    '''
    Initialise le jeu. Renvoie un nombre entier choisi aléatoirement entre 20 et 50. Ce nombre représente le nombre de pions. 
    :param:
    Aucun
    
    :renvoie:
    (list)
    
    :effets de bord:
    Aucun
    
    :exemple:
    >>> installation_jeu() in range(30,51)
    True
    '''

    return randint(30,50)

def affiche_plateau(plateau):
    '''
    affiche le nombre de pions présents sur le plateau.
    param:
    plateau:(int) le nombre de pions présents sur le plateau

    : exemples:
    >>> affiche_plateau(5)
    il reste 5 pions sur le plateau
  
    
    '''
    
    
    nb_plateau = str(plateau)
    print("il reste " + nb_plateau + " pions sur le plateau")

def premier_joueur():
    '''
    Détermine quel est le joueur débutant la partie. Renvoie 1 (joueur 1) ou 2
    (joueur 2).
    *à implémenter*
    En cas de plusieurs parties, alternance du joueur qui débute.
    
    :param:
    Aucun
    
    :renvoie:
    (int) 1 ou 2 selon le joueur.
    
    :effets de bord:
    Aucun
    
    :exemple:
    >>> premier_joueur() in [1, 2]
    True
    '''
    return randint(1, 2)

def coups_possibles(plateau, joueur_courant = None):
    '''
    Etudie les possibilités de coups possibles pour le joueur courant donné en
    paramètre. Pour le jeu de Nime, le joueur ne pourra, de toute façon, que prendre 2 ou trois pions du plateau. 
    Renvoie l'ensemble des quantités jouables sous forme d'une liste.
    
    :param:
    plateau : 
    joueur_courant : (int) valeurs théoriquemennt possibles, 1 (joueur 1) ou 2 (joueur 2)
    Par défaut, elle vaut None, n'ayant pas d'utilité pour ce jeu.
    
    :renvoie:
    (list)
    
    :effets de bord:
    Aucun
    
    :exemples:
    >>> coups_possibles(installation_jeu()) ==[2,3]
    True
    >>> coups_possibles(2) ==[2]
    True
    '''
    possibilites = []
    if plateau >2:
        possibilites.append(2)
        possibilites.append(3)
    elif plateau == 2:
        possibilites.append(2)
    return possibilites

def est_fini(plateau):
    '''
    Vérifie s'il reste au moins deux pions.
    
    :param:
    plateau : (int) représentant le nombre de pions restant sur le plateau
    :renvoie:
    (Bool)
    
    :effets de bord:
    Aucun
    
    :exemples:
    est_fini(installation_jeu())
    True
    est_fini(1)
    False
    est_fini(0)
    False
    est_fini(12)
    True
    '''
  
    return plateau<2
    
    

def peut_jouer(liste_coups):
    '''
    Détermine si le joueur en cours peut jouer ou non.
    
    :param:
    liste_coups : (list) Liste de coups à jouer valides pour le joueur en cours. 
    
    :renvoie:
    (bool)
    
    :effets de bord:
    Aucun
    
    :exemples:
    >>> peut_jouer([2.3])
    True
    >>> peut_jouer([])
    False
    '''
    return not(len(liste_coups) ==0)

def affiche_coups(liste_coups,joueur_courant):
    '''
    Affiche dans le shell le nom du joueur courant et la liste de coup qu il peut effectuer
    : param:
    liste_coups: (list) liste de coups à jouer valides pour le joueur en cours.
    joueur_courant:(int) nom du joueur en cours.
    
    :renvoie:
    (None)
    
    :effets de bord:
    Aucun
    
    '''
    joueur_Courant = str(joueur_courant)
    liste_Coups =''
    for i in range(len(liste_coups)):
        liste_Coups = liste_Coups + str(liste_coups[i]) + ','
    
    print("le joueur "+joueur_Courant+" peut jouer. La liste de ses coups possibles sont "+liste_Coups)

def coup_choisi(liste_coups):
    '''
    Demande à l'utilisateur le coup (= nombre de pions à prendre) qu'il souhaite jouer parmi ceux
    proposés.
    
    :param:
    liste_coups : (list) Liste des colonnes dans lesquelles le joueur en cours
    peut effectuer son coup.
    
    :renvoie:
    (int)
    
    :effets de bord:
    input
    
    :exemple:
    coup_choisi([2,3]) in [[2,3]
    True
    '''
    rep = 0
    while rep ==0:
        rep = int(input ('Combien de pions souhaitez vous prendre ? '))
        if not (rep in liste_coups):
            print('La colonne proposée ne peut pas être jouée.')
            rep = 0
    return rep

def mise_a_jour_plateau(coup_joue, joueur_courant, plateau):
    '''
    Modifie le plateau pour retirer le nombre de pions choisis par le joueur courant (coup_joue).
    
    :param:
    coup_joue : (int) nombre de pions que le joueur courant retire au plateau.
    joueur_courant : (int) 1 (joueur 1) ou 2 (joueur 2)
    plateau : (int) plateau représentant le jeu. entier avec le nombre de pions restant
    
    :renvoie:
    (int)
    
    :effets de bord:
    Modifie le paramètre plateau et le renvoie.
    
    :cu:
    il doit rester au moins deux pions sur le plateau
    
    :exemple:
    >>> mise_a_jour_plateau(3, 2, 40) == 37
    True
    '''
    plateau = plateau - coup_joue
    return plateau

def changement_de_joueur(joueur_courant):
    '''
    Change le joueur_courant.
    Renvoie 1 (resp. 2) si le paramètre vaut 2 (resp. 1).
    
    :param:
    joueur_courant : (int)  1 (joueur 1) ou 2 (joueur 2)
    
    :renvoie:
    (int)
    
    :effets de bord:
    Aucun
    
    :exemples:
    >>> changement_de_joueur(1) ==2
    True
    >>> changement_de_joueur(2) ==1
    True
    '''
    return (3 - joueur_courant)

def resultat_jeu(plateau,joueur_courant):
    '''
    Si un joueur est gagnant, renvoie le numéro joueur gagnant, sinon, renvoie 0
    en cas de match nul.
    
    :param:
    plateau : (int) Plateau représentant le jeu. nombre représentant le nombre de pions restant sur le plateau.
    joueur_courant: (int) joueur dont le tour est à venir
    
    :renvoie:
    (int)
    
    :effets de bord:
    Aucun
    
    :exemple:
    >>> resultat_jeu(1,2) ==0
    True
    >>> resultat_jeu(0,1) == 2
    True
    '''
    
    if plateau == 1:
        return 0
    elif joueur_courant == 1:
        return 2
    else:
        return 1
    
    
def afficher_gagnant(joueur_gagnant):
    '''
    Affiche le joueur qui a gagné la partie ou nul.
    
    :param:
    joueur_gagnant : (int) Valeurs 0, pour nul, 1 pour joueur 1 et 2 pour joueur 2.
    
    :renvoie:
    (None)
    
    :effets de bord:
    Affiche le gagnant dans le Shell.
    
    :exemples:
    >>> afficher_gagnant(1)
    Le joueur 1 a gagné !
    >>> afficher_gagnant(2)
    Le joueur 2 a gagné !
    >>> afficher_gagnant(0)
    Match nul !
    '''
    LISTE_GAGNANTS = ['Match nul !', 'Le joueur 1 a gagné !', 'Le joueur 2 a gagné !']
    print(LISTE_GAGNANTS [joueur_gagnant])
    return

if __name__ == "__main__":
    import doctest
    doctest.testmod()