#!/usr/bin/python3
# -*- coding: utf-8 -*-

#import Nime as jeu
import puissance_4_v2 as jeu

def jouer():
    plateau = jeu.installation_jeu ()
    jeu.affiche_plateau(plateau)
    joueur_courant = jeu.premier_joueur ()
    while  not jeu.est_fini (plateau):
        liste_coups = jeu.coups_possibles (plateau, joueur_courant)
        jeu.affiche_coups(liste_coups,joueur_courant)
        if jeu.peut_jouer (liste_coups):
            coup_joue = jeu.coup_choisi (liste_coups)
            plateau = jeu.mise_a_jour_plateau (coup_joue, joueur_courant, plateau)
            jeu.affiche_plateau(plateau)
        joueur_courant = jeu.changement_de_joueur (joueur_courant)
    joueur_gagnant = jeu.resultat_jeu (plateau,joueur_courant)
    jeu.afficher_gagnant (joueur_gagnant)