#!/usr/bin/python3
# -*- coding: utf-8 -*-

#########################
# importation des modules

import random


#####################################
# Définition de fonctions secondaires


def _4_pions_en_ligne (plateau):
    '''
    Vérifie si quatre pions identiques sont alignés horizontalement.
    Renvoie le numéro associé au joueur et 0 s'il n'y pas de ligne de quatre pions
    identiques.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (int)
    
    :effet de bord:
    aucun
    
    :exemple:
    >>> _4_pions_en_ligne ([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 2, 2, 2, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]) ==2
    True
    '''
    PERSONNE = 0
    AUCUN = 0
    JOUEUR1 = 1
    JOUEUR2 = 2
    
    gagnant = PERSONNE
    for ligne in plateau:
        # on parocurs les alignements de 4 pions horizontaux de gauche à droite.
        for num_colonne in range (3):
            nombre_de = [0 , 0 , 0]
            for i in range (4):
                pion = ligne[num_colonne + i]
                nombre_de [pion] += 1
            if nombre_de [0] ==AUCUN:
                if nombre_de [1] ==4:
                    gagnant = JOUEUR1
                elif nombre_de [2] ==4:
                    gagnant = JOUEUR2
    return gagnant

def _4_pions_en_colonne (plateau):
    '''
    Vérifie si quatre pions identiques sont alignés verticalement.
    Renvoie le numéro associé au joueur et 0 s'il n'y pas de ligne de quatre pions
    identiques.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (int)
    
    :effet de bord:
    aucun
    
    :exemple:
    >>> _4_pions_en_colonne ([[0, 0, 0, 0, 2, 0, 0], [0, 0, 0, 0, 2, 0, 0], [0, 0, 0, 0, 2, 0, 0], [0, 0, 1, 1, 2, 1, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]) ==2
    True
    >>> _4_pions_en_colonne ([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 2, 2, 2, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]) ==0
    True
    >>> _4_pions_en_colonne ([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 2, 2, 0, 0], [0, 0, 0, 1, 2, 0, 0], [0, 0, 1, 1, 2, 0, 0], [0, 1, 2, 1, 2, 0, 0]]) ==2
    True
    '''    
    PERSONNE = 0
    AUCUN = 0
    JOUEUR1 = 1
    JOUEUR2 = 2

    gagnant = PERSONNE
    for num_ligne in range(3):
        # on parcours les alignements verticaux de haut en bas.
        for num_colonne in range(7):
            nombre_de = [0 , 0 , 0]
            for i in range (4):
                pion = plateau[num_ligne + i] [num_colonne]
                nombre_de [pion] += 1
            if nombre_de [0] ==AUCUN:
                if nombre_de [1] ==4:
                    gagnant = JOUEUR1
                elif nombre_de [2] ==4:
                    gagnant = JOUEUR2
    return gagnant

def _4_pions_en_diagonale_mont (plateau):
    '''
    Vérifie si quatre pions identiques sont alignés en diagonale "montante" (/).
    Renvoie le numéro associé au joueur et 0 s'il n'y pas de ligne de quatre pions
    identiques.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (int)
    
    :effet de bord:
    aucun
    
    :exemple:
    >>> _4_pions_en_diagonale_mont([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 2], [0, 0, 0, 0, 0, 2, 0], [0, 0, 1, 1, 2, 1, 0], [0, 0, 0, 2, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]) ==2
    True
    '''
    PERSONNE = 0
    AUCUN = 0
    JOUEUR1 = 1
    JOUEUR2 = 2

    gagnant = PERSONNE
    for num_ligne in range(3, 6):
        # le premier pion de la diagonale vérifiée parcours les lignes 3 à 5.
        for num_colonne in range(4):
            nombre_de = [0 , 0 , 0]
            for i in range (4):
                pion = plateau[num_ligne - i] [num_colonne + i]
                nombre_de [pion] += 1
            if nombre_de [0] ==AUCUN:
                if nombre_de [1] ==4:
                    gagnant = JOUEUR1
                elif nombre_de [2] ==4:
                    gagnant = JOUEUR2
    return gagnant

def _4_pions_en_diagonale_desc (plateau):
    '''
    Vérifie si quatre pions identiques sont alignés en diagonale "montante" (/).
    Renvoie le numéro associé au joueur et 0 s'il n'y pas de ligne de quatre pions
    identiques.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (int)
    
    :effet de bord:
    aucun
    
    :exemple:
    >>> _4_pions_en_diagonale_desc([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 2, 0, 0, 0, 0, 0], [0, 0, 2, 1, 0, 1, 0], [0, 0, 0, 2, 0, 0, 0], [0, 0, 0, 0, 2, 0, 0]]) ==2
    True
    '''
    PERSONNE = 0
    AUCUN = 0
    JOUEUR1 = 1
    JOUEUR2 = 2

    gagnant = PERSONNE
    for num_ligne in range(3):
        # le premier pion de la diagonale vérifiée parcours les lignes 0 à 2.
        for num_colonne in range(4):
            nombre_de = [0 , 0 , 0]
            for i in range (4):
                pion = plateau[num_ligne + i] [num_colonne + i]
                nombre_de [pion] += 1
            if nombre_de [0] ==AUCUN:
                if nombre_de [1] ==4:
                    gagnant = JOUEUR1
                elif nombre_de [2] ==4:
                    gagnant = JOUEUR2
    return gagnant

def _y_a_t_il_un_gagnant(plateau):
    '''
    Renvoie True ou False selon s'il y a un gagnant ou non.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (bool)
    
    :effet de bord:
    Aucun
    
    :exemples:
    >>> _y_a_t_il_un_gagnant(installation_jeu())
    False
    >>> _y_a_t_il_un_gagnant([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 0]])
    True
    >>> _y_a_t_il_un_gagnant([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 0, 0, 0, 0], [0, 0, 0, 2, 0, 0, 0], [0, 0, 0, 0, 2, 0, 0], [0, 0, 0, 0, 0, 2, 0]])
    True
    >>> _y_a_t_il_un_gagnant([[1, 1, 2, 1, 1, 2, 1], [1, 1, 2, 2, 1, 2, 1], [2, 2, 2, 1, 2, 1, 2], [2, 2, 1, 2, 1, 1, 2], [1, 2, 1, 2, 2, 2, 1], [1, 1, 1, 2, 2, 1, 1]])
    False
    '''
    return ((_4_pions_en_colonne(plateau) or _4_pions_en_ligne(plateau) or
            _4_pions_en_diagonale_desc(plateau) or _4_pions_en_diagonale_mont(plateau))
            in [1, 2])

######################################
# Définition des fonctions principales

def installation_jeu ():
    '''
    Initialise le plateau de jeu. Renvoie une grille à deux dimensions contenant
    des 0 (case vide).
    
    :param:
    Aucun
    
    :renvoie:
    (list)
    
    :effets de bord:
    Aucun
    
    :exemple:
    >>> installation_jeu() == [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]
    True
    '''
    grille2d = []
    for _ in range(6):
        ligne = []
        for _ in range (7):
            ligne.append(0)
        grille2d.append(ligne)
    return grille2d

def affiche_plateau (plateau):
    '''
    Affiche dans le shell le plateau de jeu.
    Mets X pour le joueur 1, un O pour le joueur 2 et _ si la case est vide.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (None)
    
    :effet de bord:
    Affichage dans le Shell.
    
    exemple :
    >>> affiche_plateau (installation_jeu ())
    ---------------
    |0|1|2|3|4|5|6|
    ---------------
    |_|_|_|_|_|_|_|
    |_|_|_|_|_|_|_|
    |_|_|_|_|_|_|_|
    |_|_|_|_|_|_|_|
    |_|_|_|_|_|_|_|
    |_|_|_|_|_|_|_|
    ---------------
    '''
    PIONS = ['_', 'X', 'O']
    print('---------------')
    print('|0|1|2|3|4|5|6|')
    print('---------------')
    for ligne in plateau:
        print('|', end = '')
        for colonne in ligne:
            print(PIONS [colonne] + '|', end = '')
        print('')
    print('---------------')
    return

def affiche_coups (liste_coups, joueur_courant):
    '''
    Affiche le joueur qui doit jouer ainsi que les possibilités de coups valides.
    
    :param:
    liste_coups : (list) Liste d'entiers correspondant aux colonnes non pleines.
    joueur_courant : (int) valeurs théoriquemennt possibles, 1 (joueur 1) ou 2 (joueur 2)
    
    :renvoie:
    (None)
    
    :effet de bord:
    Affichages dans le Shell.
    
    :cu:
    liste_coups n'est pas une liste vide.
    
    :exemples:
    >>> affiche_coups ([0, 1, 2, 3, 4, 5, 6], 1)
    C'est au joueur 1 de jouer.
    Vous pouvez jouer dans les colonnes 0, 1, 2, 3, 4, 5, 6.
    >>> affiche_coups ([1, 3], 2)
    C'est au joueur 2 de jouer.
    Vous pouvez jouer dans les colonnes 1, 3.
    >>> affiche_coups ([4], 2)
    C'est au joueur 2 de jouer.
    Vous ne pouvez que dans la colonne 4.
    '''
    print("C'est au joueur {} de jouer.".format(joueur_courant))
    rep = ''
    nb_coups_possibles = len(liste_coups)
    # Le nombre de coups est strictement positif
    if nb_coups_possibles ==1:
        rep = 'Vous ne pouvez que dans la colonne '
    else:
        rep = 'Vous pouvez jouer dans les colonnes '
    for coup in liste_coups:
        rep = rep + str(coup) + ', '
    rep = rep[:-2] + '.'
    print (rep)
    return

def premier_joueur():
    '''
    Détermine quel est le joueur débutant la partie. Renvoie 1 (joueur 1) ou 2
    (joueur 2).
    *à implémenter*
    En cas de plusieurs parties, alternance du joueur qui débute.
    
    :param:
    Aucun
    
    :renvoie:
    (int) 1 ou 2 selon le joueur.
    
    :effets de bord:
    Aucun
    
    :exemple:
    >>> premier_joueur() in [1, 2]
    True
    '''
    return random.randint(1, 2)

def coups_possibles(plateau, joueur_courant = None):
    '''
    Etudie les possibilités de coups possibles pour le joueur courant donné en
    paramètre. Pour le puissance 4, le joueur en cours n'influence pas les
    possibilités de jeu (contrairement à Othello par exemple).
    Renvoie l'ensemble des numéros de colonnes jouables sous forme d'une liste.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    joueur_courant : (int) valeurs théoriquemennt possibles, 1 (joueur 1) ou 2 (joueur 2)
    Par défaut, elle vaut None, n'ayant pas d'utilité pour ce jeu.
    
    :renvoie:
    (list)
    
    :effets de bord:
    Aucun
    
    :exemples:
    >>> coups_possibles(installation_jeu()) ==[0, 1, 2, 3, 4, 5, 6]
    True
    >>> coups_possibles([[0, 1, 0, 0, 0, 2, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]) ==[0, 2, 3, 4, 6]
    True
    '''
    possibilites = []
    premiere_ligne = plateau [0]
    for colonne in range(7):
        elmt = premiere_ligne[colonne]
        if not elmt:
            possibilites.append(colonne)
    return possibilites

def est_fini(plateau):
    '''
    Vérifie si l'un des deux joueurs a aligné ou non quatre pions.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (Bool)
    
    :effets de bord:
    Aucun
    
    :exemples:
    >>> est_fini(installation_jeu())
    False
    >>> est_fini([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 0]])
    True
    >>> est_fini([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 0, 0, 0, 0], [0, 0, 0, 2, 0, 0, 0], [0, 0, 0, 0, 2, 0, 0], [0, 0, 0, 0, 0, 2, 0]])
    True
    >>> est_fini([[1, 1, 2, 1, 1, 2, 1], [1, 1, 2, 2, 1, 2, 1], [2, 2, 2, 1, 2, 1, 2], [2, 2, 1, 2, 1, 1, 2], [1, 2, 1, 2, 2, 2, 1], [1, 1, 1, 2, 2, 1, 1]])
    True
    '''
    plateau_plein = (len(coups_possibles(plateau)) ==0)
    return (_y_a_t_il_un_gagnant(plateau) or plateau_plein)

def peut_jouer(liste_coups):
    '''
    Détermine si le joueur en cours peut jouer ou non.
    
    :param:
    liste_coups : (list) Liste de coups à jouer valides pour le joueur en cours. 
    
    :renvoie:
    (bool)
    
    :effets de bord:
    Aucun
    
    :exemples:
    >>> peut_jouer([0, 1, 2, 3, 4, 5, 6])
    True
    >>> peut_jouer([])
    False
    '''
    return not(len(liste_coups) ==0)

def coup_choisi(liste_coups):
    '''
    Demande à l'utilisateur le coup (= colonne) qu'il souhaite jouer parmi ceux
    proposés.
    
    :param:
    liste_coups : (list) Liste des colonnes dans lesquelles le joueur en cours
    peut effectuer son coup.
    
    :renvoie:
    (int)
    
    :effets de bord:
    Affichage dans le Shell
    
    :exemple:
    coup_choisi([0, 1, 2, 3, 4, 5, 6]) in [[0, 1, 2, 3, 4, 5, 6]
    True
    '''
    rep_donnee = False
    while not rep_donnee:
        rep = int(input ('Quelle est la colonne choisie ? '))
        if not (rep in liste_coups):
            print('La colonne proposée ne peut pas être jouée.')
        else:
            rep_donnee = True
    return rep

def mise_a_jour_plateau(coup_joue, joueur_courant, plateau):
    '''
    Modifie le plateau pour y ajouter un pion du joueur_courant dans la colonne
    choisie (coup_joue).
    
    :param:
    coup_joue : (int) Numéro de colonne dans laquelle le joueur courant place sont pion.
    joueur_courant : (int) 1 (joueur 1) ou 2 (joueur 2)
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (list)
    
    :effets de bord:
    Modifie le paramètre plateau et le renvoie.
    
    :cu:
    La colonne correspondant au paramètre coup_joue ne doit pas être pleine.
    
    :exemple:
    >>> mise_a_jour_plateau(1, 2, installation_jeu ()) ==[[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 2, 0, 0, 0, 0, 0]]
    True
    '''
    VIDE = 0
    ligne_traitee = 5
    pion_place = False
    while not pion_place:
        case = plateau[ligne_traitee] [coup_joue]
        if case ==VIDE:
            plateau[ligne_traitee] [coup_joue] = joueur_courant
            pion_place = True
        else:
            ligne_traitee = ligne_traitee - 1
    return plateau

def changement_de_joueur(joueur_courant):
    '''
    Change le joueur_courant.
    Renvoie 1 (resp. 2) si le paramètre vaut 2 (resp. 1).
    
    :param:
    joueur_courant : (int)  1 (joueur 1) ou 2 (joueur 2)
    
    :renvoie:
    (int)
    
    :effets de bord:
    Aucun
    
    :exemples:
    >>> changement_de_joueur(1) ==2
    True
    >>> changement_de_joueur(2) ==1
    True
    '''
    return (3 - joueur_courant)

def resultat_jeu(plateau, joueur_courant = None):
    '''
    Si un joueur est gagnant, renvoie le numéro joueur gagnant, sinon, renvoie 0
    en cas de match nul.
    Le joueur_courant n'est pas utile pour déterminer le gagnant pour le puissance 4.
    
    :param:
    plateau : (list) Grille2d représentant le jeu. Liste de liste contenant des
    0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    joueur_courant : (int)  1 (joueur 1) ou 2 (joueur 2)
    
    :renvoie:
    (int)
    
    :effets de bord:
    Aucun
    
    :exemple:
    >>> resultat_jeu([[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 2, 2, 2, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]) ==2
    True
    '''
    return (_4_pions_en_colonne(plateau) or
            _4_pions_en_ligne(plateau) or
            _4_pions_en_diagonale_desc(plateau) or
            _4_pions_en_diagonale_mont(plateau))

def afficher_gagnant(joueur_gagnant):
    '''
    Affiche le joueur qui a gagné la partie ou nul.
    
    :param:
    joueur_gagnant : (int) Valeurs 0, pour nul, 1 pour joueur 1 et 2 pour joueur 2.
    
    :renvoie:
    (None)
    
    :effets de bord:
    Affiche le gagnant dans le Shell.
    
    :exemples:
    >>> afficher_gagnant(1)
    Le joueur 1 a gagné !
    >>> afficher_gagnant(2)
    Le joueur 2 a gagné !
    >>> afficher_gagnant(0)
    Match nul !
    '''
    LISTE_GAGNANTS = ['Match nul !', 'Le joueur 1 a gagné !', 'Le joueur 2 a gagné !']
    print(LISTE_GAGNANTS [joueur_gagnant])
    return

if __name__ == "__main__":
    import doctest
    doctest.testmod()